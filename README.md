README
------
(Last updated 1 October 2017)

## About

nemo-sendto-clamtk is a simple plugin to allow a right-click,
context menu scan of files or folders in nemo, the
file manager for Cinnamon.

All this plugin does is copy a desktop style file to the
directory where nemo reads such things.  If this seems redundant,
that's because it is: this plugin depends on clamtk,
which already has this directory file. So, it stands to
reason we should be able to just find that file and
copy it, rather than having our own copy.

nemo-sendto-clamtk is available for Fedora, CentOS, and
Debian|Ubuntu - or probably any system that can run Cinnamon.

Link to screenshot:
* http://imgur.com/YgzL7xH

## Dependencies

clamtk >= 5.00
nemo

## Important Links

For feature requests or bugs, it's best to use one of the following:

* https://github.com/dave-theunsub/nemo-sendto-clamtk
* https://bitbucket.org/davem_/nemo-sendto-clamtk/

## Contact

* Dave M, dave.nerd @gmail.com (0x6ADA59DE)
